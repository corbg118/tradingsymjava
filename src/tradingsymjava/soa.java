/**
 * This is soa.java
 * Definition of Service Oriented Architecture (SOA) Service base class
 * Translated from c++
 * @version 1.0
 */

package tradingsymjava;

import java.util.Vector;

// When a class is defined with the public keyword, it means this class is accessible and visible to all the classes in all the pakcages in Java.
// This is not necessary here as I'll only use it in tradingsymjava 

/**
 * Define base class ServiceListener to listen to add, update and remove events on a Service
 * This listener should be registered on a Service for the Service to notify all listeners for
 * these events
 * @param <V>: value
 */
abstract class ServiceListener<V> {
	
	// java is pass by value
	
	// Listener callback to process an add event to the Service
	public abstract void ProcessAdd(V data);

	// Listener callback to process an remove event to the Service
	public abstract void ProcessRemove(V data);
	
	// Listener callback to process an update event to the Service
	public abstract void ProcessUpdate(V data);
}

/**
 * Define base class Service
 * @param <K>: key type
 * @param <V>: value type
 */
abstract class Service<K, V> {
	
	// Get data on our service given a key
	public abstract V GetData(K key);
	
	// The callback that a Connector should invoke for any new or updated data
	public abstract void OnMessage(V data);
	
	// Add a listener to the Service for callbacks on add, remove and update events
	// for data to the Service.
	public abstract void AddListener(ServiceListener<V> listener);
	
	// Get all listeners on the Service
	public abstract Vector<ServiceListener<V>> GetListeners();
}

/**
 * Define base Connector class
 * This will invoke the Service.OnMessage() method for subscriber Connector to push data to the Service
 * Services can invoke the Publish() method on this Service to publish data to the Connector for a publisher Connector
 * Note that a Connector can be publisher-only, subscriber-only or both
 * @param <V>: value type
 */
abstract class Connector<V> {
	
	// Publish data to the Connector
	public abstract void Publish(V data);
}
